<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ page language="java" 
         contentType="text/html; charset=ISO-8859-1" 
         pageEncoding="ISO-8859-1"%>

<html>
    <head>
    	<title>Insurance Quote Page</title>
        <link rel="stylesheet" 
              type="text/css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function ()
    {
    	$('#quoteForm').submit(function () {
		 getQuote();
		 return false;
		});
    });

    function getQuote()
    {
    	var url = "getQuote.htm";
    	var name = $("#name").val();
    	var age= $("#age").val();
    	var gender = $("#gender").val();
    	var currentHealthArr = healthArr;
    	var currentHabitsArr = habitArr;
    	
    	$.ajax({type:"POST",
        url:url,
        data: {name:name,
               age:age,
               gender:gender,
               health:JSON.stringify(currentHealthArr),
               habit:JSON.stringify(currentHabitsArr)},
        dataType: "html",

        success : function(response)
        {
            alert("Hello " + name + ", premuim for your quote is :: " +  response);

        },
        error : function(e)
        {
            alert('Error in getting premuim: ' + e.responseText);
        }});
    }
    
    var healthArr = new Array()
    function onChangeHealth(elem)
    {
    	var idx = healthArr.indexOf(elem.id);
    	if($(elem).prop("checked") == true)
    	{
    		if(idx == -1)
    		{
    			healthArr.push(elem.id);
    		}
    	}
    	else
    	{
    		if(idx != -1)
    		{
    			healthArr.splice(idx, 1);
    		}	
    	}
    }
    
    var habitArr = new Array()
    function onChangeHabit(elem)
    {
    	var idx = habitArr.indexOf(elem.id);
    	if($(elem).prop("checked") == true)
    	{
    		if(idx == -1)
    		{
    			habitArr.push(elem.id);
    		}
    	}
    	else
    	{
    		if(idx != -1)
    		{
    			habitArr.splice(idx, 1);
    		}	
    	}
    }
    	
    </script>
    </head>
    <body>
    	<div style="background-color: #4d5e70; width: 100%; padding: 5px; font-size: 15px; color: white;font-style: normal;"><strong>Insurance Quote</strong></div>
		<form id="quoteForm" action="" onsubmit="">
		<br>
			<div align="left" style="width:100%; padding-left:20px">
				<label>Name:</label>
				<input id="name"  value=""/>
			</div>
			<br>
			<div align="left" style="width:100%; padding-left:20px;">
				<label>Age:</label>
				<input id="age" value=""/>
			</div>
			<br>
			<div align="left" style="width:100%; padding-left:20px">
				<label>Gender:</label>
                <select id="gender" style="width: 97px; font-size: 12px" class="required">
                       <option value ="" label="" selected="selected">------Select-------</option>
                       <option value ="male" label="">Male</option>
                       <option value ="female" label="">Female</option>
                </select>
			</div>
			<br>
			<div align="left" style="width:100%; padding-left:20px">
				<label>Current Health:</label><br>
				    <input id="hypertension"  type="checkbox" name="hypertension" onclick="onChangeHealth(this)" />Hypertension<br>
				    <input id="bloodPressure" type="checkbox" name="bloodPressure" onclick="onChangeHealth(this)" />Blood Pressure<br>
				    <input id="bloodSugar" type="checkbox" name="bloodSugar" onclick="onChangeHealth(this)" />Blood Sugar<br>
				    <input id="overweight" type="checkbox" name="overweight" onclick="onChangeHealth(this)" />Overweight<br>
			</div>
			<br>
			<div align="left" style="width:100%; padding-left:20px">
				<label>Habits:</label><br>
				    <input id="smoking" type="checkbox" name="smoking" onclick="onChangeHabit(this)" />Smoking<br>
				    <input id="alcohol" type="checkbox" name="alcohol" onclick="onChangeHabit(this)" />Alcohol<br>
				    <input id="exercise" type="checkbox" name="exercise" onclick="onChangeHabit(this)" />Daily Exercise<br>
				    <input id="drugs" type="checkbox" name="drugs" onclick="onChangeHabit(this)" />Drugs<br>
			</div>
			<br>
			<div align="center"><input type="submit" value="Get Quote"></div>
		</form>
	</body>
</html>