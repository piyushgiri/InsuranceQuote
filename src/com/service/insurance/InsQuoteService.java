package com.service.insurance;

import java.util.ArrayList;
import java.util.Arrays;
import com.ibm.json.java.JSON;
import com.service.insurance.InsQuoteService;

public class InsQuoteService {

	private final static String Exercise = "exercise";
	private final static String Male = "male";
	
	public InsQuoteService() {
	}
	
	public String getPremuim(
			String name,
			String age,
			String gender,
		    String health,
		    String habit)   throws Exception
	
	{
		   int basePremuim = 5000;
		   double premuim = basePremuim;

		   health = (JSON.parse(health).toString()).replace("[", "").replace("]", "").replace(" ", "");
		   habit = (JSON.parse(habit).toString()).replace("[", "").replace("]", "").replace(" ", "");;
		   
		   int personAge  = Integer.parseInt(age);
		   if(personAge > 18)
		   {
			   if(isBetween(personAge, 18, 25))
			   {
				   premuim = calculatePremium(1, premuim, 0.1);
			   }
			   else if(isBetween(personAge, 25, 30))
			   {
				   premuim = calculatePremium(2, premuim, 0.1);
			   }
			   else if(isBetween(personAge, 30, 35))
			   {
				   premuim = calculatePremium(3, premuim, 0.1);
			   }
			   else if(isBetween(personAge, 35, 40))
			   {
				   premuim = calculatePremium(4, premuim, 0.1);
			   }
			   else if(isBetween(personAge, 40, 100))
			   {
				   for(int i=40; i<personAge; i=i+5)
				   {
					   premuim = calculatePremium(1, premuim, 0.2);
				   }
			   }
		   }
		   else
		   {
			   premuim = basePremuim;
		   }
		   
		   if(gender.equalsIgnoreCase(Male))
		   {
			   premuim = premuim + premuim * 0.02;
		   }
		   
		   ArrayList<String> healthList = null;
		   ArrayList<String> habitList = null;
		   
		   if(!health.equalsIgnoreCase(""))
		   {
			   healthList = new ArrayList<String>(Arrays.asList(health.split(",")));
		   }
		   
		   if(healthList != null)
		   {
			   premuim = calculatePremium(healthList.size(), premuim, 0.01);
		   }
		   
		   if(!habit.equalsIgnoreCase(""))
		   {
			   habitList = new ArrayList<String>(Arrays.asList(habit.split(",")));
		   }
		   
		   
		   if(habitList != null)
		   {
			   int habitSize = habitList.size();
			   if(habitList.contains(Exercise))
			   {
				   premuim = premuim - premuim * 0.03;
				   habitSize = habitSize - 1;
			   }
			   
			   
			   premuim = calculatePremium(habitSize, premuim, 0.03); 	   
		   }

		   double roundOff = (double) Math.round(premuim * 100) / 100;
		   return Double.toString(roundOff);
	   }

	   private double calculatePremium(int count, double premuim, double increament)
	   {
		   if(count == 0)
		   {
			   return premuim;
		   }
		   
		   for(int i=0; i<count; i++)
		   {
			   premuim = premuim + (premuim * increament);
		   }
		   return premuim;
	   }

	   private static boolean isBetween(int value, int min, int max)
	   {
	     return((value > min) && (value < max));
	   }

}
