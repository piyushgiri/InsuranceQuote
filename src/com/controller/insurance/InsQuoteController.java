package com.controller.insurance;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.service.insurance.InsQuoteService;

@Controller
public class InsQuoteController {
	
   @Autowired
   InsQuoteService quoteService;

   /**
    * Function to set service class.
    * 
    * @param quoteService InsQuoteService class instance
    */
   public void setQuoteService(InsQuoteService quoteService)
   {
       this.quoteService = quoteService;
   }
   
   /**
    * Get Premium amount
    * 
    * @param name
    * @param age
    * @param gender
    * @param health
    * @param habit
    * @param request HttpServletRequest instance
    * @param response HttpServletResponse instance
    * 
    * @return result containing calculated premium
    * @throws Exception
    */
   @RequestMapping(method={RequestMethod.POST,RequestMethod.GET}, value="/getQuote.htm")
   public @ResponseBody String loadDecadeComments(
       @RequestParam("name") String name,
       @RequestParam("age") String age,
       @RequestParam("gender") String gender,
       @RequestParam("health") String health,
       @RequestParam("habit") String habit,
       HttpServletRequest request,
       HttpServletResponse response) throws Exception
   {
	   return quoteService.getPremuim(name, age, gender, health, habit);
   }
}
